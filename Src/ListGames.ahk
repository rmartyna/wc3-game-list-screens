;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;CONSTANTS
;only sleep times and loop number should be changed
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

WinTitle := "Warcraft III"

Error := 1

MicroSleepMs := 30
ShortSleepMs := 1000
LoopSleepMs := 10000

LoopTimes := 7

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;HELPER FUNCTIONS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

DisableInput() {
	BlockInput, On
}

EnableInput() {
	BlockInput, Off
}

MouseClick(X, Y) {
	Click, %X%, %Y%
	MicroWait()
}

KeyClick(Keys) {
	SendInput, %Keys%
	MicroWait()
}

MicroWait() {
	global MicroSleepMs

	Sleep, %MicroSleepMs%
}

ShortWait() {
	global ShortSleepMs

	Sleep, %ShortSleepMs%
}

LoopWait() {
	global LoopSleepMs

	Sleep, %LoopSleepMs%
}

Debug(Msg) {
	EnableInput()
	MsgBox, , "DEBUG", %Msg%
	DisableInput()
}

Error(Msg) {
	EnableInput()
	MsgBox, , "ERROR", %Msg%
	ExitApp, %Error%
}

CheckWindow() {
	global WinTitle

	if !WinExist(WinTitle) {
		Error("No " . WinTitle . " exists.")
	}
}

MaximizeWindow() {
	global WinTitle

	WinActivate, %WinTitle%
	WinMaximize, %WinTitle%
}

RefreshGames() {
	MouseClick(750, 115)
	ShortWait()
}

MoveToNewestGame() {
	MouseClick(700, 200)
	KeyClick("{End}")
	MouseClick(1000, 200)
}

GetGamesList() {
	global LoopTimes

	Loop %LoopTimes% {
		MoveToNextGames()
		ScreenshotGames()
	}
}

MoveToNextGames() {
	KeyClick("{PgUp}")
}

ScreenshotGames() {
	KeyClick("{PrintScreen}")
	ShortWait()
}

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;MAIN PROGRAM
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

while true {
	DisableInput()

	CheckWindow()
	MaximizeWindow()

	RefreshGames()
	MoveToNewestGame()

	GetGamesList()

	EnableInput()

	LoopWait()
}
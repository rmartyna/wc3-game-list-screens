### Requirements

* Warcraft III started with Custom Games list open.
* Assumes 1920 x 1080 resolution (otherwise coordinates need to be adjusted).
* No other window with name "Warcraft III" opened.

### How to compile

Run Bat\compile.bat script.

### How to run

Run Bin\ListGames.exe as an Administrator (otherwise some functions will not work properly).

### How it works

Application will run until explicitly closed, or until an error is encountered.

Following actions are executed in loop:
* Disables user's keyboard and mouse input (as a precaution).
* Activates and maximizes Warcraft III window.
* Refreshes games list.
* Makes screenshot each of N pages of games. A singular page has 13 games. May contain duplicated games. Screenshots should be saved into C:\Users\%User%\Documents\Warcraft III\ScreenShots path.
* Restores user's keyboard and mouse input.
* Sleeps for predefined amount of time.

Since this script requires user's mouse and keyboard input, it should be run on a separate physical or virtual machine.

Warcraft III window, if not active, sometimes ignores keyboard and mouse presses. 
Because of that, this script needs to work on active window and block user's keyboard and mouse input, so that it not interfered with.